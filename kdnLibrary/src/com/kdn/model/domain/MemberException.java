package com.kdn.model.domain;

public class MemberException extends RuntimeException {
	public MemberException(String msg){
		super(msg);
	}
}
