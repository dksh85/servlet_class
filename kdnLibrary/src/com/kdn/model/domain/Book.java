package com.kdn.model.domain;
/**
 *<pre>
 * Book 정보와 기능을 구현한 클래스
 *</pre>
 *
 * @author kdg
 * @version 1.0, 2013/06/05
 * @see 
 */
public class Book {

		/** 도서번호(ISBN) 데이터를 저장하는 변수 */
		private	String isbn;
		/** 도서제목 데이터를 저장하는 변수 */
		private		String title;
		/** 저자 데이터를 저장하는 변수 */
		private		String author;
		/** 출판사 데이터를 저장하는 변수 */
		private		String publisher;
			/** 출판일 */
		private		String pubDate;
		/** 도서 가격 데이터를 저장하는 변수 */
		private		int  price;
		/** 도서 설명 데이터를 저장하는 변수 */
		private		String info="";
			
		public Book() {
			super();
		}
		public Book(String isbn, String title, String author, String publisher,
				int price) {
			super();
			this.isbn = isbn;
			this.title = title;
			this.author = author;
			this.publisher = publisher;
			this.price = price;
		}
		
		public Book(String isbn, String title, String author, String publisher, String pubDate, int price, String info) {
			super();
			this.isbn = isbn;
			this.title = title;
			this.author = author;
			this.publisher = publisher;
			this.pubDate = pubDate;
			this.price = price;
			this.info = info;
		}

		public String getIsbn() {
			return isbn;
		}
		public void setIsbn(String isbn) {
			this.isbn = isbn;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getAuthor() {
			return author;
		}
		public void setAuthor(String author) {
			this.author = author;
		}
		public String getPublisher() {
			return publisher;
		}
		public void setPublisher(String publisher) {
			this.publisher = publisher;
		}
		public String getPubDate() {
			return pubDate;
		}
		public void setPubDate(String pubDate) {
			this.pubDate = pubDate;
		}
		public int getPrice() {
			return price;
		}
		public void setPrice(int price) {
			this.price = price;
		}
		public String getInfo() {
			return info;
		}
		public void setInfo(String info) {
			this.info = info;
		}
		/** 객체가 가지고 있는 정보를 문자열로 리턴하는 메소드*/
		public String toString(){
			return isbn+"\t| "+title+"\t| "+author+"\t| "
			+publisher+"\t| "+price+"\t| "+info;
		}
}
