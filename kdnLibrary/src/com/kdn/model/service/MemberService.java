package com.kdn.model.service;

import java.util.List;

import com.kdn.model.domain.Member;
import com.kdn.model.domain.PageBean;

public interface MemberService {
	public void add(Member member);
	public void update(Member member) ;
	public Member search(String id);
	public List<Member> searchAll( PageBean bean) ;
	public boolean login(String id, String password);
	public boolean idCheck(String id);
	public void withdraw(String id);
}











