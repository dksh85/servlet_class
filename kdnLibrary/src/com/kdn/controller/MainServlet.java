package com.kdn.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MainServlet
 */
@WebServlet("*.do")
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String content = null;
		String action = request.getServletPath();
		
		try{
			if(action.endsWith("loginForm.do")){
				content = "/member/login.jsp";
			}
		}catch(Exception e){
			content = "/errorPage.jsp";
			request.setAttribute("msg", e.getMessage());
		}
		
		request.setAttribute("content", content);
		RequestDispatcher send = request.getRequestDispatcher("/index.jsp");
		send.forward(request, response);
	}
}
