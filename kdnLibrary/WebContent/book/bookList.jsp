<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.List, com.kdn.model.domain.Book"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset=UTF-8">
<title>Insert title here</title>

</head>
<body>
	
	<table>
	<tr><td> isbn </td><td>title</td><td>author</td><td>publisher</td><td>info</td></tr>
		<c:forEach var="book" items="${list }">
			<tr><td><a href="bookView.do?isbn=${book.isbn}">${book.isbn}</a></td>
			<td><a href="bookView.do?isbn=${book.isbn}">${book.title}</a></td>
		<td>${book.author}</td>
		<td>${book.publisher}</td>
		<td>${book.info}</td>
		</tr>
		</c:forEach>
	</table>
</body>
</html>