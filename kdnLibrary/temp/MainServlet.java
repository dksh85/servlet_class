package com.kdn.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kdn.model.domain.Book;
import com.kdn.model.domain.Member;
import com.kdn.model.domain.PageBean;
import com.kdn.model.service.BookService;
import com.kdn.model.service.BookServiceImpl;
import com.kdn.model.service.MemberService;
import com.kdn.model.service.MemberServiceImpl;

/**
 * Servlet implementation class MainServlet
 */
@WebServlet("*.do")
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private MemberService memberService = new MemberServiceImpl();
	private BookService bookService = new BookServiceImpl();
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String content = null;
		String action = request.getServletPath();
		String url="index.jsp";
		System.out.println("action = " + action);
		try{
			if(action.endsWith("loginForm.do")){
				content = "/member/login.jsp";
			}
			else if(action.endsWith("login.do")){
				content = login(request, response);
			}
			else if(action.endsWith("logout.do")){
				content = logout(request, response);
			}
			else if(action.endsWith("mypage.do")){
				content = memberUpdateForm(request, response); 
			}
			else if(action.endsWith("memberUpdateForm.do")){
				content = memberUpdate(request, response);
			}
			else if(action.endsWith("bookInputForm.do")){
				content = "/book/BookInput.jsp";
			}
			else if(action.endsWith("book.do")){
				content = addBook(request, response); 
			}
			else if(action.endsWith("bookUpdateForm.do")){
				content = editBookForm(request, response);
			}
			else if(action.endsWith("bookUpdate.do")){
				content = editBook(request, response);
			}
			else if(action.endsWith("bookList.do")){
				content = bookList(request,response);
			}
			else if(action.endsWith("bookRemove.do")){
				content = bookRemove(request, response);
			}
			else if(action.endsWith("bookView.do")){
				content = bookView(request, response);
			}
		}catch(Exception e){
			request.setAttribute("msg", e.getMessage());
			content = "/errorPage.jsp";
		}
		
		request.setAttribute("content", content);
		RequestDispatcher send = request.getRequestDispatcher(url);
		send.forward(request, response);
	}

	private String bookView(HttpServletRequest request, HttpServletResponse response) {
		String isbn = (String)request.getParameter("isbn");
		Book book = bookService.search(isbn);
		request.setAttribute("book", book);
		return "/book/BookView.jsp";
	}

	private String bookRemove(HttpServletRequest request, HttpServletResponse response) {
		String isbn = request.getParameter("isbn");
		bookService.remove(isbn);
		return bookList(request, response);
	}

	private String bookList(HttpServletRequest request, HttpServletResponse response) {
		PageBean bean = new PageBean("all", null, 1);
		List<Book> list = bookService.searchAll(bean);
		request.setAttribute("list", list);
		return "/book/bookList.jsp";
	}

	private String editBook(HttpServletRequest request, HttpServletResponse response) {
		String isbn = request.getParameter("isbn");
		String title = request.getParameter("title");
		String author = request.getParameter("author");
		String publisher = request.getParameter("publisher");
		String pubDate = request.getParameter("pubDate");
		int price = Integer.parseInt(request.getParameter("price"));
		String info = request.getParameter("info");
		Book book = new Book(isbn, title, author, publisher, pubDate, price, info);
		
		bookService.update(book);
		request.setAttribute("book", book);

		return "/book/BookView.jsp";
	}

	private String editBookForm(HttpServletRequest request, HttpServletResponse response) {
		String isbn = request.getParameter("isbn");
		Book book = bookService.search(isbn);
		request.setAttribute("book", book);
		return "/book/BookUpdate.jsp";
	}

	private String addBook(HttpServletRequest request, HttpServletResponse response) {
		String isbn = request.getParameter("isbn");
		String title = request.getParameter("title");
		String author = request.getParameter("author");
		String publisher = request.getParameter("publisher");
		String pubDate = request.getParameter("publisheDate");
		int price = Integer.parseInt(request.getParameter("price"));
		String info = request.getParameter("description");
		Book book = new Book(isbn, title, author, publisher, pubDate, price, info);
		bookService.add(book);
		Book bookToShow = bookService.search(isbn);
		request.setAttribute("book", bookToShow);
		return "/book/BookView.jsp";
	}

	private String memberUpdate(HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String address = request.getParameter("address");
		
		Member member = new Member(id, password, name, email, address);
		memberService.update(member);
		Member memberToShow = memberService.search(id);
		request.setAttribute("member", memberToShow);
		return "/member/memberView.jsp";
	}

	private String memberUpdateForm(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		String id = (String)session.getAttribute("id");
		Member member = memberService.search(id);
		request.setAttribute("member", member);
	
		return "/member/MemberUpdate.jsp";
	}

	private String logout(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		session.removeAttribute("id");
		return "/member/login.jsp";
	}

	private String login(HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		String pass = request.getParameter("password");
		try{
			memberService.login(id, pass);
			HttpSession session = request.getSession();
			session.setAttribute("id", id);
		}catch(Exception e){
			request.setAttribute("msg", e.getMessage());
			return "/member/login.jsp";
		}
		return null;
	}
	
}
