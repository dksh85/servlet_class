package com.kdn.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class asdf
 */
@WebServlet("*.do")
public class asdf extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public asdf() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="index.jsp";
		String content=null;
		// getServletPath() : context 이후의 경로를 추출
		// ex) http://localhost/WebMvcProjext/login.do	=> login.do
		String action = request.getServletPath();
		try{
			System.out.printf("action>>>>>>>>>>>>>>>%s\n", action);
			if(action!=null){
				if(action.endsWith("loginForm.do")){
					content="member/login.jsp";
				}
				else if(action.endsWith("login.do")){
				}
				else if(action.endsWith("bookInputForm.do")){
					content = "book/BookInput.jsp";
				}
				else if(action.endsWith("book.do")){
				}
			}
		}catch (Exception e){
			content="errorPage.jsp";
			request.setAttribute("msg", e.getMessage());
		}
		request.setAttribute("content", content);
		RequestDispatcher send = request.getRequestDispatcher("index.jsp");
		send.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
