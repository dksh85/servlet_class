<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>KDN MVC Project</title>
<link href="css/basic.css" rel="stylesheet" type="text/css" /> 
</head>
<body>
<div id="head">
   <article id="logo"><img src="images/logo.gif"/></article>
  
<article id="content"> 
	<aside>
		<nav id="menu">		
		 <a href="insertMemberForm.do">회원가입</a><br/>
		 <a href="loginForm.do">로그인</a><br/>
		 <a href="bookInputForm.do">도서정보</a>
		</nav>
	</aside>
	<article  id="mainContent">
	     <%
	        String content = (String) request.getAttribute("content");
	     	if(content!=null){  %>
	     		<jsp:include page="<%=content %>"/>
	     	<%}else{%>
	     	    <jsp:include page="main.jsp"/>
	     	<%} %>
   </article>
</article>		
<footer id="copyright">
<jsp:include page="copyright.jsp"/>
</footer>	
</body>
</html>




