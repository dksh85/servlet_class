package util;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

/**
 * Servlet Filter implementation class EncodingFilter
 */
@WebFilter("*.do")
public class EncodingFilter implements Filter {
	public void destroy() {
	}

	/**
	 * doFilter(~) : 지정한 url에 대한 요청이 있을 때마다 수행되는 메서드
	 * 	- 필터 처리
	 * 	- 인코딩, 인증처리, 보안, 권한...
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		//	- 연결된 다른 filter나 servlet으로 이동
		//	- doFilter(~)를 호출하지 않을 경우 필터에서 응답을 해야 한다.
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
