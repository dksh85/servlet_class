package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class BookServlet
 */
@WebServlet("/book/book.do")
public class BookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public BookServlet() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String isbn = request.getParameter("isbn");
		String title = request.getParameter("title");
		String catalogue = request.getParameter("catalogue");
		String nation = request.getParameter("nation");
		String publisheDate = request.getParameter("publisheDate");
		String publisher = request.getParameter("publisher");
		String author = request.getParameter("author");
		String price = request.getParameter("price");
		String currency = request.getParameter("currency");
		String description = request.getParameter("description");
		
		PrintWriter out = response.getWriter();
		
		out.println("<!Doctype html>					   ");
		out.println("<html>                                ");
		out.println("<head>                                ");
		out.println("<meta charset='UTF-8'>                ");
		out.println("<title>로그인</title>                 ");
		out.println("</head>                               ");
		out.println("<body>                                ");
		out.println("<center> 입력된 도서 정보 </center>   ");
		out.println("<table align ='center' border = '1'>  ");
		out.println("<tr><td colspan ='2' align = 'center' width='800'>도서정보</td></tr>"  			);
		out.printf ("<tr><td width='200'>도서명</td><td>" + title + "</td></tr>"  );
		out.printf ("<tr><td width='200'>도서번호</td><td>" + isbn + "</td></tr>"  );
		out.printf ("<tr><td width='200'>도서분류</td><td>" + catalogue + "</td></tr>"  );
		out.printf ("<tr><td width='200'>도서국가</td><td>" + title + "</td></tr>"  );
		out.printf ("<tr><td width='200'>출판일</td><td>" + publisheDate + "</td></tr>"  );
		out.printf ("<tr><td width='200'>출판사</td><td>" + publisher + "</td></tr>"  );
		out.printf ("<tr><td width='200'>저자</td><td>" + author + "</td></tr>"  );
		out.printf ("<tr><td width='200'>도서가격</td><td>" + price + "</td></tr>"  );
		out.printf ("<tr><td width='200'>도서설명</td><td>" + description + "</td></tr>"  );
		
		out.println("</table> <br/>                               ");
		out.println("<a href = ./book.html><center> 도서 등록 </center></a>");
		out.println("</body>                                ");
		out.println("</html>                                ");
	}
}
