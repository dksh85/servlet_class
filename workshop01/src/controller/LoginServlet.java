package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/member/Login.do")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoginServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		String pass = request.getParameter("password");
		PrintWriter out = response.getWriter();
		
		out.println("<!doctype html>                    ");
		out.println("<html>                             ");
		out.println("<head>                             ");
		out.println("<meta charset='utf-8'>             ");
		out.println("<title>로그인</title>   ");
		out.println("</head>                            ");
		out.println("<body>                             ");
		if(id.equals("admin") && pass.equals("1111")){
			out.println("<h1><center> "+id + "님 로그인 되었습니다!! </center></h1>");
			out.println("<a href = '../book/book.html'><center> 도서등록 </center></a>"); 
		}
		else{
			out.println("<h1><center> "+ id + "로그인 실패</center></h1><br/>");
			out.println("<a href = '../member/Login.html'><center> 로그인 </center></a>");
		}
		
        out.println("                                   ");
		out.println("</body>                            ");
		out.println("</html>                            ");
	}

}
