<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
    
<%
	//session 은 내장객체
	//String id = (String)session.getAttribute("id");

%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>KDN MVC Project</title>
<link href="css/basic.css" rel="stylesheet" type="text/css" /> 
</head>
<body>
<div id="head">
   <article id="logo"><img src="images/logo.gif"/></article>
  
<article id="content"> 
	<aside>
		<nav id="menu">		
		<c:choose>
		<c:when test="${empty id}">
		 <a href="insertMemberForm.do">회원가입</a><br/>
		 <a href="loginForm.do">로그인</a><br/>
		</c:when>
		<c:otherwise>
		 <a href="logout.do">로그아웃</a>
		 <a href="mypage.do">MyPage</a>
		 <a href="bookInputForm.do">도서등록</a>
		 </c:otherwise>
		 </c:choose>
		 <a href="bookList.do">도서목록</a>
		</nav>
	</aside>
	<article id="mainContent">
	
	<c:choose>
		<c:when test="${!empty content }">
	     		<jsp:include page="${content}"/>
		</c:when>	
		<c:otherwise>
	     	    <jsp:include page="main.jsp"/>
		</c:otherwise>
	</c:choose>	
   </article>
</article>		
<footer id="copyright">
<jsp:include page="copyright.jsp"/>
</footer>	
</body>
</html>




