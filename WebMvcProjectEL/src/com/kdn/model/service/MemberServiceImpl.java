package com.kdn.model.service;

import java.sql.Connection;
import java.util.List;

import com.kdn.model.dao.MemberDao;
import com.kdn.model.dao.MemberDaoImpl;
import com.kdn.model.domain.Member;
import com.kdn.model.domain.MemberException;
import com.kdn.model.domain.PageBean;
import com.kdn.util.DBUtil;

public class MemberServiceImpl implements MemberService {
	private MemberDao dao = new MemberDaoImpl();
	public void add(Member member) {
		Connection con = null;
		try {
			con = DBUtil.getConnection();
			con.setAutoCommit(false);
			Member find = dao.search(con, member.getId());
			if(find != null){
				throw new MemberException("해당 아이디는 이미 등록되어 있습니다.");
			}
			dao.add(con, member);
		}catch(MemberException e){
			DBUtil.rollback(con);
			throw e;
		}catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback(con);
			throw new MemberException("회원 등록 중 오류 발생");
		}finally{
			DBUtil.close(con);
		}
	}
	public void update(Member member) {
		Connection con = null;
		try {
			con = DBUtil.getConnection();
			con.setAutoCommit(false);
			Member find = dao.search(con, member.getId());
			if(find == null){
				throw new MemberException("아이디에 해당하는 회원을 찾을 수 없어 수정할 수 없습니다.");
			}
			member.setWithdraw("N");
			dao.update(con, member);
		}catch(MemberException e){
			DBUtil.rollback(con);
			throw e;
		}catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback(con);
			throw new MemberException("회원 등록 중 오류 발생");
		}finally{
			DBUtil.close(con);
		}
	}
	public Member search(String id) {
		Connection con = null;
		try {
			con = DBUtil.getConnection();
			Member find = dao.search(con, id);
			if(find == null){
				throw new MemberException("아이디에 해당하는 회원을 찾을 수 없습니다.");
			}
			return find;
		}catch(MemberException e){
			throw e;
		}catch (Exception e) {
			e.printStackTrace();
			throw new MemberException("회원 검색 중 오류 발생");
		}finally{
			DBUtil.close(con);
		}
	}
	
	public List<Member> searchAll(PageBean bean) {
		Connection con = null;
		try {
			con = DBUtil.getConnection();
			con.setAutoCommit(false);
			int total = dao.getCount(con, bean);
			bean.setTotal(total);
			return dao.searchAll(con, bean);
		}catch(MemberException e){
			throw e;
		}catch (Exception e) {
			e.printStackTrace();
			throw new MemberException("회원 등록 중 오류 발생");
		}finally{
			DBUtil.close(con);
		}
	}
	public boolean login(String id, String password) {
		Connection con = null;
		try {
			con = DBUtil.getConnection();
			Member find = dao.search(con, id);
			if(find !=null){
				if(password.equals(find.getPassword())){
					if(find.getWithdraw().equals("N")){
						return true;
					}else{
						throw new MemberException("이미 탈퇴한 아이디입니다.");
					}
				}else{
					throw new MemberException("비밀 번호 오류");
				}
			}else{
				throw new MemberException("해당 아이디는 등록되지 않았습니다.");
			}
		}catch(MemberException e){
			throw e;
		}catch (Exception e) {
			e.printStackTrace();
			throw new MemberException("회원 검색 중 오류 발생");
		}finally{
			DBUtil.close(con);
		}
	}
	public boolean idCheck(String id) {
		Connection con = null;
		try {
			con = DBUtil.getConnection();
			Member find = dao.search(con, id);
			if(find !=null){
				return true;
			}else{
				return false;
			}
		}catch (Exception e) {
			e.printStackTrace();
			throw new MemberException("회원 검색 중 오류 발생");
		}finally{
			DBUtil.close(con);
		}
	}
	public void withdraw(String id) {
		Connection con = null;
		try {
			con = DBUtil.getConnection();
			con.setAutoCommit(false);
			Member find = dao.search(con, id);
			if(find == null){
				throw new MemberException("아이디에 해당하는 회원을 찾을 수 없습니다.");
			}
			find.setWithdraw("Y");
			dao.update(con, find);
		}catch(MemberException e){
			DBUtil.rollback(con);
			throw e;
		}catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback(con);
			throw new MemberException("회원 탈퇴 중 오류 발생");
		}finally{
			DBUtil.close(con);
		}
	}
}
