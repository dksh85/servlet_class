package com.kdn.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.kdn.model.domain.Book;
import com.kdn.model.domain.PageBean;
import com.kdn.util.DBUtil;

public class BookDaoImpl implements BookDao {
	public void add(Connection con, Book book) throws SQLException{
			PreparedStatement  stmt = null;
			try {
				String sql = " insert into book (isbn, title, author, publisher, pubDate, price, info) "
						+ " values (?,?,?,?,?,?,? ) ";
				stmt = con.prepareStatement(sql);
				stmt.setString(1, book.getIsbn());
				stmt.setString(2, book.getTitle());
				stmt.setString(3, book.getAuthor());
				stmt.setString(4, book.getPublisher());
				stmt.setString(5, book.getPubDate());
				stmt.setInt(6, book.getPrice());
				stmt.setString(7, book.getInfo());
				stmt.executeUpdate();
			} finally{
				DBUtil.close(stmt);
			}			
	}
	public void update(Connection con, Book book) throws SQLException{
		PreparedStatement  stmt = null;
		try {
			String sql = " update book set title=?, author=?, publisher=?, pubDate=?, price=?, info=?  "
					+ " where isbn=? ";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, book.getTitle());
			stmt.setString(2, book.getAuthor());
			stmt.setString(3, book.getPublisher());
			stmt.setString(4, book.getPubDate());
			stmt.setInt(5, book.getPrice());
			stmt.setString(6, book.getInfo());
			stmt.setString(7, book.getIsbn());
			stmt.executeUpdate();
		} finally{
			DBUtil.close(stmt);
		}			
	}

	public void remove(Connection con, String isbn) throws SQLException{
		PreparedStatement  stmt = null;
		try {
			String sql = " delete from book where isbn=? ";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, isbn);
			stmt.executeUpdate();
		} finally{
			DBUtil.close(stmt);
		}			
	}

	public Book search(Connection con, String isbn)throws SQLException {
		PreparedStatement  stmt = null;
		ResultSet  rs = null;
		try {
			String sql = " select isbn, title, author, publisher " 
					         + "        , to_char(pubdate, 'yy-mm-dd') pubdate , price, info "
				         	+ " from book "
				         	+ " where isbn=? ";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, isbn);
			rs = stmt.executeQuery();
			if(rs.next()){
				return new Book(rs.getString("isbn")
																		, rs.getString("title")
																		, rs.getString("author")
																		, rs.getString("publisher")
																		, rs.getString("pubDate")
																		, rs.getInt("price")
																		, rs.getString("info"));
			}
		} finally{
			DBUtil.close(rs);
			DBUtil.close(stmt);
		}			
		return null;
	}

	public List<Book> searchAll(Connection con, PageBean bean)throws SQLException{
		PreparedStatement  stmt = null;
		ResultSet  rs = null;
		try {
			 String key = bean.getKey();
			 String word = bean.getWord();
				StringBuilder  sql = new StringBuilder(100);
				sql.append(" SELECT  a.*                                                 \n");
				sql.append(" FROM   ( SELECT   ROW_NUMBER() OVER (ORDER BY isbn desc) ro \n");
				sql.append("                  , isbn, title, author, price                \n");
				sql.append("                  , TO_CHAR(pubdate, 'yy-mm-dd') pubdate     \n");
				sql.append("          FROM    book                                       \n");
				sql.append("          WHERE  1 = 1                                       \n");
				if( key!=null && !key.equals("all") && word!=null && !word.equals("")){
					if(key.equals("author")){
						  sql.append("   and author = ?   ");
					}else if(key.equals("publisher")){
						sql.append("   and publisher  like  ?   ");
					}else if(key.equals("title")){
						sql.append("   and title  like  ?   ");
					}
				}
				sql.append("          ) a                                                \n");
				sql.append(" WHERE   ro BETWEEN ?   AND  ?                               \n");
				stmt = con.prepareStatement(sql.toString());
				int idx = 1; 
				if( key!=null && !key.equals("all") && word!=null && !word.equals("")){
					if(key.equals("author")){
						stmt.setString(idx++, word);
					}else {
						stmt.setString(idx++,"%"+word+"%");
					}
				}
				stmt.setInt(idx++, bean.getStart());
				stmt.setInt(idx++, bean.getEnd());
				rs = stmt.executeQuery();
				ArrayList<Book> books = new ArrayList<Book>(10);
				while(rs.next()){
				books.add( new Book(rs.getString("isbn")
																						, rs.getString("title")
																						, rs.getString("author")
																						, rs.getString("pubDate")
																						, rs.getInt("price")));
    } 
				return books;
		} finally{
			DBUtil.close(rs);
			DBUtil.close(stmt);
		}			
	}

	public int getCount(Connection con, PageBean bean) throws SQLException{
		PreparedStatement  stmt = null;
		ResultSet  rs = null;
		try {
		 String key = bean.getKey();
		 String word = bean.getWord();
			StringBuilder  sql = new StringBuilder(100);
			sql.append(" SELECT   count(*) cnt  FROM book WHERE  1 = 1 \n");
			if( key!=null && !key.equals("all") && word!=null && !word.equals("")){
				if(key.equals("author")){
					  sql.append("   and author = ?   ");
				}else if(key.equals("publisher")){
					sql.append("   and publisher  like  ?   ");
				}else if(key.equals("title")){
					sql.append("   and title  like  ?   ");
				}
			}
			stmt = con.prepareStatement(sql.toString());
			int idx = 1; 
			if( key!=null && !key.equals("all") && word!=null && !word.equals("")){
				if(key.equals("author")){
					stmt.setString(idx++, word);
				}else {
					stmt.setString(idx++,"%"+word+"%");
				}
			}
			rs = stmt.executeQuery();
			ArrayList<Book> books = new ArrayList<Book>(10);
			if(rs.next()){
				return  rs.getInt("cnt");
			}
		} finally{
			DBUtil.close(rs);
			DBUtil.close(stmt);
		}			
		return 0;
	}
}
