package com.kdn.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.InitialContext;
import javax.sql.DataSource;

public class DBUtil {
	private static final String DRIVER = "oracle.jdbc.driver.OracleDriver";
	private static final String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	private static final String USER = "scott";
	private static final String PASSWORD = "tiger";
	private static DataSource env;
	static{
		  try {
			  env = (DataSource) new InitialContext().lookup("java:comp/env/jdbc/oracle");  
		  } catch (Exception e ) { 
		  	try{Class.forName(DRIVER);}catch(Exception e1){}
		  } 
	}
	public 	static Connection getConnection() throws SQLException{
		System.out.println("env..."+env);
		if(env ==null){
			return DriverManager.getConnection(URL, USER, PASSWORD);
		}else{
			return env.getConnection();
		}
	}
	public static void rollback(Connection con){
		if(con != null){
			try {
				con.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	public static void close(Connection con){
		if(con != null){
			try {
				if(!con.getAutoCommit()){
					con.commit();
					con.setAutoCommit(true);
				}
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	public static void close(Statement stmt){
		if(stmt != null){
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}	
	}
	public static void close(ResultSet rs){
		if(rs != null){
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}












