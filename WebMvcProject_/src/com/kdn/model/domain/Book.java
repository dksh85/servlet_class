package com.kdn.model.domain;

public class Book {
	private String title;
	private String isbn;
	private String catalogue;
	private String nation;
	private String publisheDate;
	private String publisher;
	private String author;
	private String price;
	private String currency;
	private String description;
	
	
	
	
	public Book(String title, String isbn, String catalogue, String nation, String publisheDate, String publisher,
			String author, String price, String currency, String description) {
		super();
		this.title = title;
		this.isbn = isbn;
		this.catalogue = catalogue;
		this.nation = nation;
		this.publisheDate = publisheDate;
		this.publisher = publisher;
		this.author = author;
		this.price = price;
		this.currency = currency;
		this.description = description;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getCatalogue() {
		return catalogue;
	}
	public void setCatalogue(String catalogue) {
		this.catalogue = catalogue;
	}
	public String getNation() {
		return nation;
	}
	public void setNation(String nation) {
		this.nation = nation;
	}
	public String getPublisheDate() {
		return publisheDate;
	}
	public void setPublisheDate(String publisheDate) {
		this.publisheDate = publisheDate;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}
