package com.kdn.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kdn.model.domain.Book;
import com.kdn.model.service.MemberService;
import com.kdn.model.service.MemberServiceImpl;

/**
 *  모든 요청을 처리하는 Servlet
 *  1. 각각의 요청 url을 분석해서 해당 url을 처리하는 메서드로 분기
 *  2. 메서드에서 처리한 결과를 request에서 담아서 지정한 url로 forward
 *  3. error가 발생시 error 메세지를 request에 저장해서 ErrorPage.jsp로 forward
 */
@WebServlet("*.do")
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public MainServlet() {
        super();
    }

    private MemberService memberService = new MemberServiceImpl();

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url="index.jsp";
		String content=null;
		// getServletPath() : context 이후의 경로를 추출
		// ex) http://localhost/WebMvcProjext/login.do	=> login.do
		String action = request.getServletPath();
		try{
			System.out.printf("action>>>>>>>>>>>>>>>%s\n", action);
			if(action!=null){
				if(action.endsWith("loginForm.do")){
					content="member/login.jsp";
				}
				else if(action.endsWith("login.do")){
					content = login(request, response);
				}
				else if(action.endsWith("bookInputForm.do")){
					content = "book/BookInput.jsp";
				}
				else if(action.endsWith("book.do")){
					content = createBook(request, response);
				}
			}
		}catch (Exception e){
			content="errorPage.jsp";
			request.setAttribute("msg", e.getMessage());
		}
		request.setAttribute("content", content);
		RequestDispatcher send = request.getRequestDispatcher("index.jsp");
		send.forward(request, response);
	}

	private String login(HttpServletRequest request, HttpServletResponse response) {
		// 1. 요청 정보 추출 request.getParameter(String name)
		String id = request.getParameter("id");
		String password = request.getParameter("password");
		
		try{
			memberService.login(id, password);
			return "book/BookInput.jsp";
		} catch (Exception e){
			request.setAttribute("msg", e.getMessage());
			return "member/login.jsp";
		}
		// 2. model 수행
		// 3. model 수행 결과를 request에 담는다.
	}
	
	public String createBook(HttpServletRequest request, HttpServletResponse response){
		Book b = new Book(request.getParameter("title")
						, request.getParameter("isbn")
						, request.getParameter("catalogue")
						, request.getParameter("nation")
						, request.getParameter("publisheDate")
						, request.getParameter("publisher")
						, request.getParameter("author")
						, request.getParameter("price")
						, request.getParameter("currency")
						, request.getParameter("description"));
		
		request.setAttribute("book", b );
		
		return "book/BookView.jsp";
	}
}
