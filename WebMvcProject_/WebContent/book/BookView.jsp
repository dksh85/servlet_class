<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import= "com.kdn.model.domain.Book" %>
<!DOCTYPE html>
<html>
<head>
<meta charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<% Book book = (Book)request.getAttribute("book"); %>
	<table align="center" border ="1">
	<tr><td colspan = "2" align="center" width="300"> 도서 정보 </td></tr>
	<tr><td align="center"> 도서명 </td>
		<td align="center" width="300"> 
			<%=book.getTitle() %>
		</td></tr>
	<tr><td> 도서번호 </td>
		<td align="center" width="300"><%=book.getIsbn() %>
		</td></tr>
	<tr><td> 도서분류 </td>
		<td align="center" width="300"><%=book.getCatalogue() %>
		</td></tr>
	<tr><td> 도서국가 </td>
		<td align="center" width="300"><%=book.getNation() %>
		</td></tr>
	<tr><td> 출판일 </td>
		<td align="center" width="300"><%=book.getPublisheDate() %>
		</td></tr>	
	<tr><td> 출판사 </td>
		<td align="center" width="300"><%=book.getPublisher() %>
		</td></tr>
	<tr><td> 저자 </td>
		<td align="center" width="300"><%=book.getAuthor() %>
		</td></tr>
	<tr><td> 도서가격 </td>
		<td align="center" width="300"><%=book.getPrice() %> <%=book.getCurrency() %>
		</td></tr>
	<tr><td> 도서설명 </td>
		<td align="center" width="300"><%=book.getDescription() %>
		</td></tr>
	</table>	
	
	<a href=bookInputForm.do><center>도서 등록</center></a>
	

	
</body>
</html>