<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kdn.model.domain.Member"%>
    
    
<% Member member = (Member)request.getAttribute("member"); %>

<!DOCTYPE html>
<html>
<head>
<meta charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
function memberUpdate(){
	var frm = document.getElementById("bookForm");
	frm.submit();
}
function  formReset(){
	var frm = document.getElementById("bookForm");
	frm.reset();
}
</script>
</head>
<body>
<form method="post" action="memberUpdateForm.do" id="bookForm">
	<input type="hidden" name="id" value="<%=member.getId()%>"/>
	<!--도서아이디정보 입력 테이블 시작//-->
	<table cellpadding="0" cellspacing="1" border="0" width="650" bgcolor="#EAEAEA" align="center">
	<tr><td width="650" bgcolor="#FFFFFF" height="35"  valign="middle" align="right" colspan="2">
		회원 수정 </td></tr>
		
	<tr><td align="center"> 아이디 </td>
		<td bgcolor="#FFFFFF"><%=member.getId()%></td>
		
		
	<tr><td align="center"> 비밀번호 </td>
	<td bgcolor="#FFFFFF">
		<input type="text" value="<%=member.getPassword()%>" name="password" >
	</td></tr>

	<tr><td align="center"> 이름 </td>
	<td bgcolor="#FFFFFF">
		<input type="text" value="<%=member.getName()%>" name="name" >
	</td></tr>

	<tr><td align="center"> 이메일 </td>
	<td bgcolor="#FFFFFF">
		<input type="text" value="<%=member.getEmail()%>" name="email">
	</td></tr>

	<tr><td align="center"> 주소 </td>
	<td bgcolor="#FFFFFF">
		<input type="text" value="<%=member.getAddress()%>" name="address">
	</td></tr>
	
	</table>
<br>
	<table cellpadding="0" cellspacing="1" border="0" width="650" bgcolor="#EAEAEA" align="center">
		<tr>
			<td align="center" height="30" valign="middle">
				<a href="javascript:memberUpdate()"><img src="images\ok.gif" width="64" height="29" vspace="12" hspace="2" alt="확인"  border="0" tabindex="19" border="0"></a>
				<a href="javascript:formReset()"><img src="images\cancel.gif" width="64" height="29" vspace="12" hspace="2" alt="취소" tabindex="20" border="0" ></a>
			</td>
		</tr>
	</table>
</body>
</html>