<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kdn.model.domain.Member"%>
    
    
<% Member member = (Member)request.getAttribute("member"); %>

<!DOCTYPE html>
<html>
<head>
<meta charset=UTF-8">
<title>Insert title here</title>

</head>
<body>
<form method="post" action="memberUpdateForm.do" id="bookForm">
	<input type="hidden" name="id" value="<%=member.getId()%>"/>
	<!--도서아이디정보 입력 테이블 시작//-->
	<table cellpadding="0" cellspacing="1" border="0" width="650" bgcolor="#EAEAEA" align="center">
	<tr><td width="650" bgcolor="#FFFFFF" height="35"  valign="middle" align="right" colspan="2">
		회원 정보 </td></tr>
		
	<tr><td align="center"> 아이디 </td>
		<td bgcolor="#FFFFFF"><%=member.getId()%></td>
	</table>


	<tr><td align="center"> 비밀번호 </td>
		<td bgcolor="#FFFFFF"><%=member.getPassword()%></td>
	</table>

	<tr><td align="center"> 이름 </td>
		<td bgcolor="#FFFFFF"><%=member.getName()%></td>
	</table>	

	<tr><td align="center"> 이메일 </td>
		<td bgcolor="#FFFFFF"><%=member.getEmail()%></td>
	</table>	
	
	<tr><td align="center"> 주소 </td>
		<td bgcolor="#FFFFFF"><%=member.getAddress()%></td>
	</table>
	
<br>

</body>
</html>