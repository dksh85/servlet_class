create table book(
  isbn      varchar2 (15) primary key,
  title     varchar2 (100),
  author    varchar2 (30),
  publisher varchar2 (100),
  pubdate   date,
  price     number,
  info      varchar2 (4000)
);
  
select * from book;