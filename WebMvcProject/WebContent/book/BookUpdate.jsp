<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kdn.model.domain.Book"%>
    
<% Book book = (Book)request.getAttribute("book"); %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
function bookUpdate(){
	var frm = document.getElementById("bookForm");
	frm.submit();
}
function  formReset(){
	var frm = document.getElementById("bookForm");
	frm.reset();
}
</script>
</head>
<body >
<!--도서등록 타이틀 테이블 시작//-->
<table width="650" cellpadding="0" cellspacing="0"  align="center" >
	<tr bgcolor="#E3E3E3" height=25>
		<td width="700"  valign="middle" ></td>
	</tr>
	<tr bgcolor="#E3E3E3">
		<td width="700"  valign="middle" >
			<center><h2>도서 등록 화면</h2></center>
		</td>
	</tr>
</table>
<!--도서등록 타이틀 끝//-->
<p>
<font color="#000000" size="3" face="굴림체">
<!--도서정보입력 FORM 테이블시작//-->
<form method="post" action="bookUpdate.do" id="bookForm">
	<input type="hidden" name="isbn" value="<%=book.getIsbn()%>"/>
	<!--도서아이디정보 입력 테이블 시작//-->
	<table cellpadding="0" cellspacing="1" border="0" width="650" bgcolor="#EAEAEA" align="center">
		<!-- 필수입력필드표기 -->
		<tr>
			<td width="650" bgcolor="#FFFFFF" height="35"  valign="middle" align="right" colspan="2">
				<img src="images\star_orange.gif" hspace="7" vspace="3">표시가 된 항목은 필수 입력 항목입니다.
			</td>
		</tr>	
		<tr>
			<td width="650" bgcolor="#F3F3F3" height="35" colspan="2">
			</td>
		</tr>
		<!--도서번호 입력-->
		<tr>
			<td bgcolor="#FFFFFF" height="35" width="120" valign="middle">
				<img src="images\star_orange.gif" hspace="7" vspace="3">도서번호
			</td>
			<td bgcolor="#FFFFFF" height="35"><%=book.getIsbn()%>
			</td>			
		</tr>	
		<!--도서제목-->
		<tr>
			<td bgcolor="#FFFFFF" height="35" width="120" valign="middle">
				<img src="images\star_orange.gif" hspace="7" vspace="3">도서제목
			</td>
			<td bgcolor="#FFFFFF" height="35">&nbsp;&nbsp;
				<span class="info">
					<input type="text" value="<%=book.getTitle()%>" name="title" size="50" MAXLENGTH="50" tabindex="2">&nbsp;
				</span>
			</td>			
		</tr>	
  

		<!--출판일-->
		<tr>
			<td bgcolor="#FFFFFF" height="35" width="120">
				<img src="images\square_gray.gif" hspace="7" vspace="3">&nbsp;출&nbsp;판&nbsp;일
			</td>
			<td bgcolor="#FFFFFF" height="35">&nbsp;&nbsp;
				<span id="publisheDate">
					<input type='text' value="<%=book.getPubDate()%>" name='publisheDate' size='15'   tabindex="6"> 
				</span>	
				</td>
		</tr>
		<!--출판사 -->
		<tr>
			<td bgcolor="#FFFFFF" height="35" width="120">
				<img src="images\square_gray.gif" hspace="7" vspace="3">&nbsp;출&nbsp;판&nbsp;사
			</td>
			<td bgcolor="#FFFFFF" height="35">&nbsp;&nbsp;
				<span id="publisher"> 
<!-- 바로 입력으로 바꿈 -->
					<input type="text" value="<%=book.getPublisher()%>" name="publisher" />
				</span>	
			</td>			
		</tr>	
		<!-- 저자 -->
		<tr>
			<td bgcolor="#FFFFFF" height="35" width="120" valign="middle">
				<img src="images\star_orange.gif" hspace="7" vspace="3">저&nbsp;&nbsp;&nbsp;&nbsp;자
			</td>
			<td bgcolor="#FFFFFF" height="35">&nbsp;&nbsp;
				<span class="info">
					<input type="text" name="author" value="<%=book.getAuthor()%>" size="10" MAXLENGTH="50" tabindex="8">&nbsp;
				</span>
			</td>			
		</tr>	
		<!-- 도서가격 -->
		<tr>
			<td bgcolor="#FFFFFF" height="35" width="120" valign="middle">
				<img src="images\square_gray.gif" hspace="7" vspace="3">&nbsp;도서가격
			</td>
			<td bgcolor="#FFFFFF" height="35">&nbsp;&nbsp;
				<span class="info">
					<input type="text" value="<%=book.getPrice()%>" name="price" size="10" MAXLENGTH="6" tabindex="9">&nbsp;
				</span>
				<select name="currency" tabindex="10">
					<option value="원" selected>원</option>
					<option value="달러">달러</option>
				</select>
			</td>			
		</tr>	
		<!--요약내용-->
		<tr>
			<td bgcolor="#FFFFFF" height="60" width="120">
				<img src="images\square_gray.gif" hspace="7" vspace="3">&nbsp;요약내용
			</td>
			<td>&nbsp;&nbsp;
				<textarea name="description" rows="3" cols="50" tabindex="11"><%=book.getInfo()%></textarea>
			</td>			
		</tr>	
	</table>
	<!--기본입력 테이블 끝//-->

	<br>
	<table cellpadding="0" cellspacing="1" border="0" width="650" bgcolor="#EAEAEA" align="center">
		<tr>
			<td align="center" height="30" valign="middle">
				<a href="javascript:bookUpdate()"><img src="images\ok.gif" width="64" height="29" vspace="12" hspace="2" alt="확인"  border="0" tabindex="19" border="0"></a>
				<a href="javascript:formReset()"><img src="images\cancel.gif" width="64" height="29" vspace="12" hspace="2" alt="취소" tabindex="20" border="0" ></a>
			</td>
		</tr>
	</table>
</form>
<!--정보입력 테이블끝//-->
</font>
<center>
<font COLOR="NAVY" SIZE="2" FACE="굴림체">
사전 도움말 - 이용약관 - 개인정보취급방침 - 책임의 한계와 법적고지 - 고객센터<br/> 
Copyright ⓒ 2013. KDN. All Rights Reserved.
</font>
</center>
</body>
</html>

