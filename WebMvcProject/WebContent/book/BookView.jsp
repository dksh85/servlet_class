<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kdn.model.domain.*"  %>
<%
	Book book = (Book) request.getAttribute("book");
%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
function bookInsert(){
	var frm = document.getElementById("bookForm");
	frm.submit();
}
function  formReset(){
	var frm = document.getElementById("bookForm");
	frm.reset();
}
</script>
</head>
<body >
<!--도서등록 타이틀 테이블 시작//-->
<table width="650" cellpadding="0" cellspacing="0"  align="center" >
	<tr bgcolor="#E3E3E3" height=25>
		<td width="700"  valign="middle" ></td>
	</tr>
	<tr bgcolor="#E3E3E3">
		<td width="700"  valign="middle" >
			<center><h2>도서 정보 화면</h2></center>
		</td>
	</tr>
</table>
<!--도서등록 타이틀 끝//-->
<p>
<font color="#000000" size="3" face="굴림체">
	<!--도서아이디정보 입력 테이블 시작//-->
	<table cellpadding="0" cellspacing="1" border="0" width="650" bgcolor="#EAEAEA" align="center">
		<!-- 필수입력필드표기 -->
		<tr>
			<td width="650" bgcolor="#FFFFFF" height="35"  valign="middle" align="right" colspan="2">
				<img src="images\star_orange.gif" hspace="7" vspace="3">표시가 된 항목은 필수 입력 항목입니다.
			</td>
		</tr>	
		<tr>
			<td width="650" bgcolor="#F3F3F3" height="35" colspan="2">
			</td>
		</tr>
		<!--도서번호 입력-->
		<tr>
			<td bgcolor="#FFFFFF" height="35" width="120" valign="middle">
				<img src="images\star_orange.gif" hspace="7" vspace="3">도서번호
			</td>
			<td bgcolor="#FFFFFF" height="35"><%=book.getIsbn()%></td>			
		</tr>	
		<!--도서제목-->
		<tr>
			<td bgcolor="#FFFFFF" height="35" width="120" valign="middle">
				<img src="images\star_orange.gif" hspace="7" vspace="3">도서제목
			</td>
			<td bgcolor="#FFFFFF" height="35"><%=book.getTitle()%></td>			
		</tr>	
		<!--출판일-->
		<tr>
			<td bgcolor="#FFFFFF" height="35" width="120">
				<img src="images\square_gray.gif" hspace="7" vspace="3">&nbsp;출&nbsp;판&nbsp;일
			</td>
			<td bgcolor="#FFFFFF" height="35"><%=book.getPubDate()%></td>
		</tr>
		<!--출판사 -->
		<tr>
			<td bgcolor="#FFFFFF" height="35" width="120">
				<img src="images\square_gray.gif" hspace="7" vspace="3">&nbsp;출&nbsp;판&nbsp;사
			</td>
			<td bgcolor="#FFFFFF" height="35"><%=book.getPublisher()%></td>			
		</tr>	
		<!-- 저자 -->
		<tr>
			<td bgcolor="#FFFFFF" height="35" width="120" valign="middle">
				<img src="images\star_orange.gif" hspace="7" vspace="3">저&nbsp;&nbsp;&nbsp;&nbsp;자
			</td>
			<td bgcolor="#FFFFFF" height="35"><%=book.getAuthor()%></td>			
		</tr>	
		<!-- 도서가격 -->
		<tr>
			<td bgcolor="#FFFFFF" height="35" width="120" valign="middle">
				<img src="images\square_gray.gif" hspace="7" vspace="3">&nbsp;도서가격
			</td>
			<td bgcolor="#FFFFFF" height="35"><%=book.getPrice()%>원</td>			
		</tr>	
		<!--요약내용-->
		<tr>
			<td bgcolor="#FFFFFF" height="60" width="120">
				<img src="images\square_gray.gif" hspace="7" vspace="3">&nbsp;요약내용
			</td>
			<td><%=book.getInfo()%></td>			
		</tr>	
	</table>
	<!--기본입력 테이블 끝//-->
	<br>
	<table cellpadding="0" cellspacing="1" border="0" width="650" bgcolor="#EAEAEA" align="center">
		<tr>
			<td align="center" height="30" valign="middle">
				<a href="bookUpdateForm.do?isbn=<%=book.getIsbn()%>">수정</a>
				<a href="bookRemove.do?isbn=<%=book.getIsbn()%>">삭제</a>
			</td>
		</tr>
	</table>
<!--정보입력 테이블끝//-->
</font>
<center>
<font COLOR="NAVY" SIZE="2" FACE="굴림체">
사전 도움말 - 이용약관 - 개인정보취급방침 - 책임의 한계와 법적고지 - 고객센터<br/> 
Copyright ⓒ 2013. KDN. All Rights Reserved.
</font>
</center>
</body>
</html>

