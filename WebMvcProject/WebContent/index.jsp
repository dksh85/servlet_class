<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
    
<%
	//session 은 내장객체
	String id = (String)session.getAttribute("id");

%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>KDN MVC Project</title>
<link href="css/basic.css" rel="stylesheet" type="text/css" /> 
</head>
<body>
<div id="head">
   <article id="logo"><img src="images/logo.gif"/></article>
  
<article id="content"> 
	<aside>
		<nav id="menu">		
		<%if(id == null){	//인증안한 상황 %>
		 <a href="insertMemberForm.do">회원가입</a><br/>
		 <a href="loginForm.do">로그인</a><br/>
		 <%} else { //인증이 된 경우 %>

		 <a href="logout.do">로그아웃</a>
		 <a href="mypage.do">MyPage</a>
		 <a href="bookInputForm.do">도서등록</a>
		 <% } %>
		 <a href="bookList.do">도서목록</a>
		</nav>
	</aside>
	<article id="mainContent">
	     <%
	        String content = (String) request.getAttribute("content");
	     	if(content!=null){  %>
	     		<jsp:include page="<%=content %>"/>
	     	<%}else{%>
	     	    <jsp:include page="main.jsp"/>
	     	<%} %>
   </article>
</article>		
<footer id="copyright">
<jsp:include page="copyright.jsp"/>
</footer>	
</body>
</html>




