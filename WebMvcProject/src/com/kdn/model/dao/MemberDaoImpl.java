package com.kdn.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.kdn.model.domain.Member;
import com.kdn.model.domain.PageBean;
import com.kdn.util.DBUtil;

public class MemberDaoImpl implements MemberDao {
	public void add(Connection con, Member member) throws SQLException {
		PreparedStatement stmt = null;
		try {
			String sql = " insert into member (id, password, name, email, address)"
					       + " values(?,?,?,?,?) ";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, member.getId());
			stmt.setString(2, member.getPassword());
			stmt.setString(3, member.getName());
			stmt.setString(4, member.getEmail());
			stmt.setString(5, member.getAddress());
			stmt.executeUpdate();
		} finally {
			DBUtil.close(stmt);
		}
	}
	public void update(Connection con, Member member) throws SQLException {
		PreparedStatement stmt = null;
		try {
			String sql = " update member set password=?, name=?, email=? "
					       + " , address=?, withdraw=? "
					       + " where id = ? ";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, member.getPassword());
			stmt.setString(2, member.getName());
			stmt.setString(3, member.getEmail());
			stmt.setString(4, member.getAddress());
			stmt.setString(5, member.getWithdraw());
			stmt.setString(6, member.getId());
			stmt.executeUpdate();
		} finally {
			DBUtil.close(stmt);
		}
	}
	public void remove(Connection con, String id) throws SQLException {
		PreparedStatement stmt = null;
		try {
			String sql = " delete from member where id = ? ";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, id);
			stmt.executeUpdate();
		} finally {
			DBUtil.close(stmt);
		}
	}
	public Member search(Connection con, String id) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String sql = " select * from member where id= ?  ";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, id);
			rs = stmt.executeQuery();
			if(rs.next()){
				return new Member(id
												, rs.getString("password")
												, rs.getString("name")
												, rs.getString("email")
												, rs.getString("address")
												, rs.getString("withdraw"));
			}
		} finally {
			DBUtil.close(stmt);
		}
		return null;
	}
	public List<Member> searchAll(Connection con, PageBean bean) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String key = bean.getKey();
			String word = bean.getWord();
			StringBuilder sql = new StringBuilder(100) ;
			sql.append(" select a.*                                        \n");
			sql.append(" from (select row_number() over (order by name) ro \n");
			sql.append("               ,id, name, email, address           \n");
			sql.append("         from member                               \n");
			sql.append("         where 1=1                                 \n");
			if(!key.equals("all") && !word.trim().equals("") ){
				if(key.equals("name")){
					sql.append("     and  name  like     ?          ");
				}else if(key.equals("email")){
						sql.append("     and  email  like  ?          ");
				}else if(key.equals("address")){
					sql.append("     and  address  like  ?          ");
				}
			}
			sql.append("       ) a                                         \n");
			sql.append(" where ro between ? and ?                          \n");
			stmt = con.prepareStatement(sql.toString());
			int idx = 1;
			if(!key.equals("all") && !word.trim().equals("") ){
				stmt.setString(idx++, "%"+word+"%");
			}
			stmt.setInt(idx++, bean.getStart());
			stmt.setInt(idx++, bean.getEnd());
			rs = stmt.executeQuery();
			ArrayList<Member> members = 
					                 new ArrayList<Member>(bean.getInterval());
			while(rs.next()){
				members.add( new Member(rs.getString("id")
												      , rs.getString("name")
												      , rs.getString("email")
												      , rs.getString("address")));
			}
			return members;
		} finally {
			DBUtil.close(rs);
			DBUtil.close(stmt);
		}
	}
	
	public int getCount(Connection con, PageBean bean) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String key = bean.getKey();
			String word = bean.getWord();
			StringBuilder sql = new StringBuilder(100) ;
			sql.append(" select count(*) cnt from member \n");
			sql.append(" where 1=1                       \n");
			if(!key.equals("all") && !word.trim().equals("") ){
				if(key.equals("name")){
					sql.append("     and  name  like     ?          ");
				}else if(key.equals("email")){
						sql.append("     and  email  like  ?          ");
				}else if(key.equals("address")){
					sql.append("     and  address  like  ?          ");
				}
			}
			stmt = con.prepareStatement(sql.toString());
			int idx = 1;
			if(!key.equals("all") && !word.trim().equals("") ){
				stmt.setString(idx++, "%"+word+"%");
			}
			rs = stmt.executeQuery();
			if(rs.next()){
				return rs.getInt("cnt");
			}
		} finally {
			DBUtil.close(rs);
			DBUtil.close(stmt);
		}
		return 0;
	}
}









