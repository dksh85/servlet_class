package com.kdn.model.service;

import java.sql.Connection;
import java.util.List;

import com.kdn.model.dao.BookDao;
import com.kdn.model.dao.BookDaoImpl;
import com.kdn.model.domain.Book;
import com.kdn.model.domain.BookException;
import com.kdn.model.domain.PageBean;
import com.kdn.util.DBUtil;

public class BookServiceImpl implements BookService {
	private BookDao  dao = new BookDaoImpl();
	public void add(Book book) {
		Connection  con = null;
		try {
			con = DBUtil.getConnection();
			con.setAutoCommit(false);
			Book find = dao.search(con, book.getIsbn());
			if(find!=null) throw new BookException("이미 등록된 isbn번호 입니다.");
			dao.add(con, book);
		} catch (BookException e) {
				DBUtil.rollback(con);
				throw e;
		} catch (Exception e) {
			DBUtil.rollback(con);
			throw new BookException("책 정보 저장 중 오류 발생");
		}finally{
			DBUtil.close(con);
		}
	}
	
	public void update(Book book) {
		Connection  con = null;
		try {
			con = DBUtil.getConnection();
			con.setAutoCommit(false);
			Book find = dao.search(con, book.getIsbn());
			if(find == null) throw new BookException("isbn에 해당하는 책이 없어 수정할 수 없습니다.");
			dao.update(con, book);
		} catch (BookException e) {
				DBUtil.rollback(con);
				throw e;
		} catch (Exception e) {
			DBUtil.rollback(con);
			throw new BookException("책 정보 수정 중 오류 발생");
		}finally{
			DBUtil.close(con);
		}
	}
	public void remove(String isbn) {
		Connection  con = null;
		try {
			con = DBUtil.getConnection();
			con.setAutoCommit(false);
			Book find = dao.search(con, isbn);
			if(find == null) throw new BookException("isbn에 해당하는 책이 없어 삭제할 수 없습니다.");
			dao.remove(con, isbn);
		} catch (BookException e) {
				DBUtil.rollback(con);
				throw e;
		} catch (Exception e) {
			e.printStackTrace();
			DBUtil.rollback(con);
			throw new BookException("책 정보 삭제 중 오류 발생");
		}finally{
			DBUtil.close(con);
		}
	}

	public Book search(String isbn) {
		Connection  con = null;
		try {
			con = DBUtil.getConnection();
			Book find = dao.search(con, isbn);
			if( find ==null) throw new BookException("isbn에 해당하는 책 정보를 찾을 수 없습니다.");
			return find;
		}catch (BookException e) {
			throw e;
		}catch (Exception e) {
			e.printStackTrace();
			throw new BookException("책 정보 검색 중 오류 발생");
		}finally{
			DBUtil.close(con);
		}
	}

	public List<Book> searchAll(PageBean bean) {
		Connection  con = null;
		try {
			con = DBUtil.getConnection();
			con.setAutoCommit(false);
			int total = dao.getCount(con, bean);
			bean.setTotal(total);
			return dao.searchAll(con, bean);
		} catch (BookException e) {
				throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new BookException("책 정보 검색 중 오류 발생");
		}finally{
			DBUtil.close(con);
		}
	}
}
