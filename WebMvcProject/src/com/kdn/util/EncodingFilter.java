package com.kdn.util;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

/**
 * Servlet Filter implementation class EncodingFilter
 */
@WebFilter("*.do")
public class EncodingFilter implements Filter {
	public void destroy() {	}
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		/* doFilter(~) :
		 * 요청 URL에 연결된 다음 Filter or Servlet을 수행시키는 메서드   
		 */
		chain.doFilter(request, response);
	}
	public void init(FilterConfig fConfig) throws ServletException { }

}
