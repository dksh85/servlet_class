package com.kdn.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kdn.model.domain.Book;
import com.kdn.model.domain.BookException;
import com.kdn.model.domain.Member;
import com.kdn.model.domain.PageBean;
import com.kdn.model.service.BookService;
import com.kdn.model.service.BookServiceImpl;
import com.kdn.model.service.MemberService;
import com.kdn.model.service.MemberServiceImpl;

import sun.rmi.log.LogOutputStream;

/**
 *  모든 요청을 처리하는 Servlet
 *  1. 각각의 요청 url을 분석해서 해당 url을 처리하는 메서드로 분기
 *  2. 메서드에서 처리한 결과를 request에 담아서 지정한 url로 forward
 *  3. error가 발생시 error 메세지를 request에 저장해서 errorPage.jsp로 forword
 */
@WebServlet("*.do")
public class MainServlet extends HttpServlet {
	private MemberService memberService = new MemberServiceImpl();
	private BookService bookService = new BookServiceImpl();
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String content=null;   
		//1. context 이후의 요청 경로를 추출
		//ex) /WebMvcProject/login.do	==> login.do를 추출
		String action = request.getServletPath();
		String url = "index.jsp";
		//2. try catch
		try{
			//4
			if( action.endsWith("loginForm.do")){
				content="member/login.jsp";

			}
			else if(action.endsWith("login.do")){
				content = login(request, response);
			}
			else if(action.endsWith("bookInputForm.do")){
				content = "book/BookInput.jsp";
			}
			else if(action.endsWith("book.do")){
				content = BookRegist(request, response);
			}
			else if(action.endsWith("bookUpdateForm.do")){ //도서정보 화면 수정
				content = bookUpdateForm(request, response); 
			}
			else if(action.endsWith("bookUpdate.do")){ //도서정보 수정
				content = bookUpdate(request, response);
			}
			else if(action.endsWith("bookList.do")){ //도서정보 수정
				content = bookList(request, response);
			}
			else if(action.endsWith("bookView.do")){
				content = bookView(request,response);
			}
			else if(action.endsWith("mypage.do")){
				content = memberUpdateForm(request, response);
			}
			else if(action.endsWith("memberUpdateForm.do")){
				content = memberUpdate(request,response);
			}
			else if(action.endsWith("logout.do")){
				content = logout(request,response);
			}
			
			//--4
			//--4
		} catch(Exception e){
			// 오류 내용을 화면에 표시하기 위한 처리
			content = "errorPage.jsp";
			request.setAttribute("msg", e.getMessage());
		}
		request.setAttribute("content", content);
		//3. forward를 위한 기본처리
		/*	RequestDispatcher : 동일 컨텍스트 내의 자원을 활용할 수 있는 객체
		 *		include(request, response) : 컨텍스트 내의 jsp, html, txt... 등을 include 
		 *		forward(request, response) : 컨텍스트 내의 jsp, servlet으로 이동
		 */
		RequestDispatcher send = request.getRequestDispatcher(url);
		send.forward(request, response);
	}
	
	private String logout(HttpServletRequest request, HttpServletResponse response) {
		/*
		 * 	invalidate()로 세션을 만료시킴으로 로그아웃 처리
		 * 	: session에 저장한 모든 정보는 해제됨.
		 */
		HttpSession session = request.getSession();
//		session.invalidate();
		
		/*
		 * 	removeAttribute(~)를 통해 인증 정보를 제거해서 로그아웃 처리
		 * 	: 인증 정보 외에 session에 저장한 다른 정보를 유지 됨.
		 */
		
		session.removeAttribute("id");
		
		return null;
	}
	private String memberUpdate(HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String address = request.getParameter("address");

		Member member = new Member(id, password, name, email, address);
		memberService.update(member);
		
		request.setAttribute("member", member);
		return "member/memberView.jsp";
	}
	private String memberUpdateForm(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		String id = (String)session.getAttribute("id");
		Member member = memberService.search(id);
		request.setAttribute("member", member);
		return "member/MemberUpdate.jsp";
	}
	private String bookView(HttpServletRequest request, HttpServletResponse response) {
		String isbn = (String)request.getParameter("isbn");
		Book book = bookService.search(isbn);
		request.setAttribute("book", book);
		return "book/BookView.jsp";
	}
	private String bookList(HttpServletRequest request, HttpServletResponse response) {
		
		
		List<Book> list = bookService.searchAll(new PageBean("all", null, 1));
		request.setAttribute("list", list);
		return "book/bookList.jsp";
	}
	
	
	
	private String bookUpdate(HttpServletRequest request, HttpServletResponse response) {
		String isbn = request.getParameter("isbn");
		String title = request.getParameter("title");
		String author = request.getParameter("author");
		String publisher = request.getParameter("publisher");
		String pubDate = request.getParameter("publisheDate");
		String priceValue = request.getParameter("price");
		String info = request.getParameter("description");
		
		int price = 0;
		
		try{
			price = Integer.parseInt(priceValue);
		} catch (Exception e){
			throw new BookException("도서가격은 정수로 입력해 주세요");
		}
		Book book = new Book(isbn, title, author, publisher, pubDate, price, info);
		bookService.update(book);
		
		request.setAttribute("book", book);
		
		return "book/BookView.jsp";
	}
	private String login(HttpServletRequest request, HttpServletResponse response) {
		// 1. 요청 정보 추출    request.getParameter(String name)
		String id = request.getParameter("id");
		String password = request.getParameter("password");
		// 2. model 수행
		try {
			memberService.login(id, password);
			//밑으로 떨어지면 인증이 된 상황
			//id, password가 모두 맞는 상황이므로 인증 정보를 session에 저장
			HttpSession session = request.getSession();
			session.setAttribute("id", id);
			
			return null;
		} catch (Exception e) {
			//3. 결과를 request에 저장
			request.setAttribute("msg", e.getMessage());
			//4. UI 화면으로 이동
			return "member/login.jsp";
		}
	}
	
	private String BookRegist(HttpServletRequest request, HttpServletResponse response){
		String isbn = request.getParameter("isbn");
		String title = request.getParameter("title");
		String author = request.getParameter("author");
		String publisher = request.getParameter("publisher");
		String pubDate = request.getParameter("publisheDate");
		String priceValue = request.getParameter("price");
		String info = request.getParameter("description");
		
		int price = 0;
		
		try{
			price = Integer.parseInt(priceValue);
		} catch (Exception e){
			throw new BookException("도서가격은 정수로 입력해 주세요");
		}
		Book book = new Book(isbn, title, author, publisher, pubDate, price, info);
		bookService.add(book);
		
		request.setAttribute("book", book);
		
		return "book/BookView.jsp";
		
	}
	
	String bookUpdateForm(HttpServletRequest request, HttpServletResponse response){
String isbn = (String)request.getParameter("isbn");
		Book book = bookService.search(isbn);
		request.setAttribute("book", book);
		return "book/BookUpdate.jsp";
	}
}