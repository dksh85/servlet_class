<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

   <%
  		String[] price={"3000", "5000", "2000"};
  		pageContext.setAttribute("price", price);
   %> 



<!DOCTYPE html>
<html>
<head>
<meta charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<c:forEach var="p" items="${price}">${p}</c:forEach>
	<br />
	<c:forEach var="p" items="${price}" begin="1" end="3">${p}</c:forEach>
	<c:forEach var="p" items="${price}" varStatus="cnt">
	${cnt.count} : ${p} <br />
	</c:forEach>
	<br />

	<c:set var="total" value="0" />

	<c:forEach var="p" items="${price}">
		<c:set var="total" value="${total+p}" />
	</c:forEach>
	total price: ${total}
	<br />	
	<c:set var="t2" value="0"/>
	<c:forEach begin="1" end="2" >
		<c:set var="t2" value="${t2+1}"/>
		t2: ${t2}
	<br />	
	</c:forEach>

<c:set var="greeting" value="hi,hello,goodMorning"/>
<c:forEach var="p" items="${greeting}">${p}<br/></c:forEach>
</body>
</html>