<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<!-- String user = request.getParameter("user"); ${param.user} -->
<!-- String user = request.getAttribute("user"); $ {user} -->
<!-- == equals를 호출해준다 -->
<c:choose>
	<c:when test="${param.user=='member'}">
		<c:import url="../member/login.html"/>
	</c:when>
	<c:when test="${param.user=='guest'}">
		<c:import url="../member/memberRegister.html"/>
	</c:when>
	<c:otherwise>
		다시 입력하세요
	</c:otherwise>
</c:choose>

<c:out value=""></c:out>
<br/><a href="ChooseTest.html">다시하기</a>
</body>
</html>