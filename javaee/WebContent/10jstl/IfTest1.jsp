<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<c:if test="${param.user=='member'}">
	<c:import url="../member/login.html"/>
</c:if>
<c:if test="${param.user=='guest'}">
	<c:import url="../member/memberRegister.html"/>
</c:if>
<c:if test="${param.user!='guest' && param.user!='member'}">
	다시 입력하세요
</c:if>
<br/><a href="Iftest1.html">다시하기</a>
</body>
</html>