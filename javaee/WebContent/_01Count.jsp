<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset=UTF-8">
<title>JSP 첫번째 예제</title>
</head>
<body>
	<%! //선언부 : 속성과 메서드선언 가능 
		private int count = 0;
		public void test(){ }
	%>
	<% // _jspService(~)에 추가되는 java code %>
	<h1> 방문자 수 : <%= ++count %></h1>
</body>
</html>