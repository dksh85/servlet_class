package domain;

import java.io.Serializable;

public class Product implements Serializable{
	private String proname;
	private int proprice;
	private int quantity;
	
	public Product(){
	}

	public String getProname() {
		return proname;
	}

	public void setProname(String proname) {
		this.proname = proname;
	}

	public int getProprice() {
		return proprice;
	}

	public void setProprice(int proprice) {
		this.proprice = proprice;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	
	public boolean equals(Object o){
		if(o instanceof Product){
			Product p = (Product) o;
			if(proname !=null && proname.equals(p.proname)){
				return true;
			}
		}
		return false;
	}
	@Override
	public String toString() {
		return "Product [proname=" + proname + ", proprice=" + proprice + ", quantity=" + quantity + "]";
	}
	
	
}
