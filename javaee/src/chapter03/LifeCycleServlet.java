package chapter03;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*@WebServlet("/url")
 * 	: servlet에 대한 url mapping 설정
 * 	LifeCycleServlet(servlet)은 객체가 하나 생성되어 여러 쓰레드를 통해 공유된다
 * 
 *	Servlet : 멀티 쓰레드에 의해 공유되는 공유 객체
 *		- servlet에서 선언한 속성은 멀티 쓰레드에 의해 공유되는 공유 변수
 */
@WebServlet("/lifecycle.do")
public class LifeCycleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//웬만하면 공유변수 만들지 말자
	
	/** init() : 서블릿 객체가 생성된 후 한번 호출되는 메서드
	 * 			 서블릿에 대한 초기화
	 * 	container 가 기본생성자를 통해 객체를 생성하기 때문에 생성자를 구현하면 에러남
	 * 	초기화는 init을 통해	
	 */
	public void init() throws ServletException{
		ServletContext ctx = getServletContext();
		// ctx.getInitParameter("param.name")
		// web.xml에 설정한 param 정보를 추출하는 메서드
		
		System.out.printf("driver : %s\n", ctx.getInitParameter("driver"));
		System.out.println("init() call ");
	}
       
//    public LifeCycleServlet() {
//        super();
//    }

    /**
     * service(HttpServletRequest request, HttpServletResponse response)
     *  : 클라이언트의 요청이 있을 때마다 수행되는 메서드
     *    클라이언트의 요청을 처리하는 메서드
     */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("service() call ");
		System.out.println("잘되나...? ");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	
	/**
	 *  destroy() : 서브릿이 unload될 때 한번 수행되는 메서드
	 *  - 사용한 자원 반납
	 */
	public void destroy() {
		System.out.println("destroy() call");
	}
}
