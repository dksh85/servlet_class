package chapter03;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Product;

/**
 * Servlet implementation class HelloServlet
 * init(), service() call을 위해 
 */
@WebServlet("/hello.do")
public class HelloServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    Product  p = new Product();
    public HelloServlet() {
        super();
    }
    
    /**
     *  HttpServletRequest : 클라이언트의 요청에 대한 모든 정보를 담고 있는 객체 (클라이언트의 요청정보를 담고있음)
     *  HttpServletResponse : 클라이언트에게 응답을 하기위한 모든 정보를 담고 있는 객체 (응답을 위한 정보)
     */

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Date date = new Date();
	//	response.getWriter().append("Served at: ").append(request.getContextPath());
	//	content 설정을 하지 않으면 기본적으로 text/html; charset = 'ISO-8859-1'
		response.setContentType("text/xml");
//		response.setContentType("application/vnd.ms-excel");
	//	한글 처리를 위해
		response.setCharacterEncoding("UTF-8"); //한글 지원을 위해
	//	클라이언트에게 응답하기 위한 출력 객체 추출
	//	out 객체를 통해 출력되는 모든 정보는 client에게 출력된.
		PrintWriter out = response.getWriter();

		
		out.println(" <!DOCTYPE html>                       ");
		out.println(" <html>                                ");
		out.println(" <head>                                ");
//		out.println(" <meta charset='utf-8'> 				");	// " 을  '으로
		out.println(" <title>insert title here</title>      ");
		out.println(" </head>                               ");
		out.println(" <body>                                ");
        out.println("   <h1> Hello Servlet </h1>            ");
        out.println("   <h2> 현재시간: 	            ");
        out.println(	date.toLocaleString()				 );
		out.println(" </h2>	</body>                         ");
		out.println(" </html>                               ");
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
