package chapter04;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/formpost.do")
public class ParamForPostServlet extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		사용자기 입력한 데이터에 대한 한글 처리
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");
		String password = request.getParameter("password");
		String gender = request.getParameter("gender");
		String job = request.getParameter("job");
		String content = request.getParameter("content");
		String[] hobby = request.getParameterValues("hobby");
		
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter(); 
		
		out.println("<!DOCTYPE html>                      ");
		out.println("<html>                               ");
		out.println("<head>                               ");
		out.println("<meta charset='UTF-8'>               ");
		out.println("<title>Insert title here</title>     ");
		out.println("</head>                              ");
		out.println("<body>  입력된 데이터 정보 <br/>     ");
		out.printf("아이디 : %s <br/> ", id);
		out.printf("비밀번호 : %s <br/> ", password);
		out.printf("성별: %s <br/> ",	gender);
		out.printf("직업: %s <br/> ", job);
		out.printf("직업: %s <br/> ", job);
		out.println("취미: ");
		if(hobby!= null){
			for (String ho : hobby){
				out.printf("%s ", ho);
			}
		}
		
		out.println("<br/>");
		out.printf("하고 싶은 말 : %s <br/> ", content);

		out.println("</body></html>");
	}
}
