package chapter04;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MemberInsertServlet
 */
@WebServlet("/member/memberinsert.do")
public class MemberInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public MemberInsertServlet() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		request.setCharacterEncoding("UTF-8");
//		response.setCharacterEncoding("UTF-8");
		
		
		String id = request.getParameter("id");
		String pw = request.getParameter("pw");
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		
		PrintWriter out = response.getWriter();
		
		out.println("<!DOCTYPE html>                       ");
		out.println("<html>                                ");
		out.println("<head>                                ");
		out.println("<meta charset='UTF-8'>                ");
		out.println("<link href='../css/basic.css' rel='stylesheet' type='text/css'> ");
		out.println("<title>Insert title here</title>      ");
		out.println("</head>                               ");
		out.println("<body>                                ");
        out.println(" <center> 입력한 회원 정보 </center>  ");
        out.println("<table align ='center' border='1'>");
        out.println("<tr><th class ='tbasic'> 아 이 디 </th><td>");
        out.printf("%s</td></tr>", id);
        out.println("<tr><th class ='tbasic'> 비밀번호 </th><td>");
        out.printf("%s</td></tr>", pw);
        out.println("<tr><th class ='tbasic'> 이 름 </th><td>");
        out.printf("%s</td></tr>", name);
        out.println("<tr><th class ='tbasic'> 이 메 일 </th><td>");
        out.printf("%s</td></tr>", email);
        out.println("</table>");
        out.println("<a href='login.html'> 로그인 </a>");
		out.println("</body>                               ");
		out.println("</html>                               ");
	}

}
